import {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import Api from "@/api/api";
import { Session } from "@/models/session";

class ParticipantApi extends Api {

    public constructor(config: AxiosRequestConfig) {
        super(config);
    }

    public finishSession(session: Session): Promise<string> {
        return this.get<string>(`finish/${session.id}`)
            .then((response: AxiosResponse) => {
                return response.data;
            })
            .catch((error: AxiosError) => {
                throw error;
            })
    }
}

export const finishApi = new ParticipantApi({});