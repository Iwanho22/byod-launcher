using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ByodLauncher.Hubs;
using ByodLauncher.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ByodLauncher.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FinishController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ByodLauncherContext _context;
        private readonly IHubContext<SessionHub, ISessionHub> _sessionHub;

        public FinishController(IConfiguration configuration, ByodLauncherContext context, IHubContext<SessionHub, ISessionHub> sessionHub)
        {
            _configuration = configuration;
            _context = context;
            _sessionHub = sessionHub;
        }

        [HttpGet("{sessionId}")]
        public async Task<IActionResult> FinsihSession(Guid sessionId)
        {
            var session = await GetSession(sessionId);

            if (!string.IsNullOrEmpty(session.Director.ConnectionId))
            {
                await _sessionHub.Clients.All.ReceiveSessionFinished();
            }
            return Ok();
        }

        private async Task<Session> GetSession(Guid sessionId)
        {
            return await _context.Sessions
                .Include(s => s.Director)
                .SingleOrDefaultAsync(s => s.Id == sessionId);
        }
    }
}